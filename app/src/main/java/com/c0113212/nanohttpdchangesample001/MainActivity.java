package com.c0113212.nanohttpdchangesample001;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    EditText textitem001;
    EditText textitem002;
    EditText textitem003;
    private static final String FILE_NAME = "ChangeHTML";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //EditText取得
        textitem001 =(EditText)findViewById(R.id.change01);
        textitem002 =(EditText)findViewById(R.id.change02);
        textitem003 =(EditText)findViewById(R.id.change03);
        //NanoHTPPDStatr
        Button serberstart =(Button)findViewById(R.id.serverstart);
        serberstart.setOnClickListener(new ServerStart());
        //Webサイトの表記変更
        Button changehtml =(Button)findViewById(R.id.changehtml);
        changehtml.setOnClickListener(new ChangeHtml());
    }


    //Serverを作る処理
    class ServerStart implements View.OnClickListener {
        // onClickメソッド(ボタンクリック時イベントハンドラ)
        public void onClick(View v) {

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("NanoHttpd")
                    .setMessage("起動しました")
                    .setPositiveButton("OK", null)
                    .show();
            try {
                WebServer wb =new WebServer();
                wb.start();
            } catch (IOException e) {
            }
        }
    }


    //HTML編集
    class ChangeHtml implements View.OnClickListener {
        // onClickメソッド(ボタンクリック時イベントハンドラ)
        public void onClick(View v) {

            try{
                //ファイルの内容削除
                deleteFile(FILE_NAME);
            }catch(Exception e){
            }
            try{
                FileOutputStream stream
                        = openFileOutput(FILE_NAME, MODE_APPEND);
                BufferedWriter out
                        = new BufferedWriter(new OutputStreamWriter(stream));

                //一個目のエディットテキスト内容確認
                if(textitem001.getText().toString().equals("") == false){
                    out.write(textitem001.getText().toString());
                    out.newLine();
                }
                else {
                    Log.d("１つ目の項目の中身","ないんな");
                    out.write("中身なし");
                    out.newLine();
                }
                //２個目のエディットテキスト内容確認
                if(textitem002.getText().toString().equals("") == false){
                    out.write(textitem002.getText().toString());
                    out.newLine();
                }
                else {
                    Log.d("２つ目の項目の中身","ないんな");
                    out.write("中身なし");
                    out.newLine();
                }
                //３個目のエディットテキスト内容確認
                if(textitem003.getText().toString().equals("") == false){
                    out.write(textitem003.getText().toString());
                    out.newLine();
                }
                else {
                    Log.d("３つ目の項目の中身","ないんな");
                    out.write("中身なし");
                    out.newLine();
                }
                out.close();
                Log.d("データ格納","成功");
            }catch(Exception e){

            }
        }
    }

    private class WebServer extends NanoHTTPD {
        public WebServer() throws IOException {
            super(8080);
        }

        @Override
        public Response serve(IHTTPSession session) {


            // メッセージ表示用
            String str = "";
            String changemsg01 = null;
            String changemsg02 = null;
            String changemsg03 = null;
            int txtcheck = 0;
            String tmp01 = null, tmp02 = null, tmp03 = null;

            // ファイルからデータ取得
            try{
                FileInputStream stream
                        = openFileInput(FILE_NAME);
                BufferedReader in
                        = new BufferedReader(new InputStreamReader(stream));

                String line = "";
                while((line = in.readLine())!=null){
                    str = line;
                    if(str.indexOf("中身なし")!=-1){
                        txtcheck++;
                    }
                    else{
                        if(txtcheck==0) {
                            changemsg01 =str;
                            txtcheck++;
                        }
                        else if(txtcheck==1) {
                            changemsg02 =str;
                            txtcheck++;
                        }
                        else if(txtcheck==2) {
                            changemsg03 =str;
                            txtcheck++;
                        }
                    }
                }
                Log.d("ソースに格納する前のチェック",changemsg01+changemsg02+changemsg03);
                in.close();
            }catch(Exception e){
            }

            String msg01 = "<!DOCTYPE html>\n" +
                    "<html lang=\"ja\">\n" +
                    "\n" +
                    "<head>\n" +
                    "    <meta charset=\"utf-8\">\n" +
                    "    <title>TM Lib : Editor</title>\n" +
                    "</head>" +
                    "<body>\n";

            if (changemsg01 != null) {
                Log.d("呼び出し","01");
                tmp01 = changemsg01 + "    <br>\n" +
                        "    <form name=\"test01\">\n" +
                        "        <textarea name=\"txt\" rows=\"10\" cols=\"50\"></textarea>\n" +
                        "    </form>\n";
            }
            else {
                Log.d("項目１は","ぬるぽです");
            }
            if (changemsg02!=null) {
                Log.d("呼び出し","02");
                tmp02 = changemsg02 + "    <br>\n" +
                        "    <form name=\"test02\">\n" +
                        "        <textarea name=\"txt\" rows=\"10\" cols=\"50\"></textarea>\n" +
                        "    </form>\n";

            }
            else {
                Log.d("項目２は","ぬるぽです");
            }
            if (changemsg03!=null) {
                Log.d("呼び出し","03");
                tmp03 = changemsg03 + "    <br>\n" +
                        "    <form name=\"test03\">\n" +
                        "        <textarea name=\"txt\" rows=\"10\" cols=\"50\"></textarea>\n" +
                        "    </form>\n";

            }
            else {
                Log.d("項目３は","ぬるぽです");
            }


            //HTML作成
            if(changemsg01==null){
                return newFixedLengthResponse(msg01 + "</body></html>\n");
            }
            else if(changemsg02==null){
                return newFixedLengthResponse(msg01 + tmp01 + "</body></html>\n");

            }
            else if(changemsg03==null){
                return newFixedLengthResponse(msg01 + tmp01 + tmp02+ "</body></html>\n");

            }
            else {
                return newFixedLengthResponse(msg01 + tmp01 + tmp02+tmp03+ "</body></html>\n");
            }

        }
    }
}
